Live demo
=========
https://mailapi-yuri.herokuapp.com

Prerequisites
-------------
 - Python 3.6

Installation
=============

    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt
    export PYTHONPATH=.
    python mailapi/manage.py migrate
    python mailapi/manage.py runserver

Puzzle content
=============

Attached are some messages as they are stored on the server.  Because
spam-filters will probably reject them, they've been put into a
zipfile which is encrypted. Password is 'spam'.

Please create a Python3 / Django based server that exposes these
messages as JSON, while doing some parsing.

Note they are not in a database, but for this exercise you can of
course just store them in SQLite if that makes it easier to integrate
with a Django model. In that case, please make sure that the database
contains raw data, please don't pre-render. (If you do, please include
software to convert raw maildir to database!  )

I would expect a (json) list of message-ids for a GET request for
/messages/ .

I would expect something like the following json to be returned for a
GET request for /messages/{message-id}

    {
    'message-id': '<message-id@from-header>',
    'from': { 'address': 'some@example.com',
              'name': 'Some Name'
              },
    'to': [ { ... }, ... ],
    'subject': '...',
    'raw-headers': [ ['Received', '...'], ... ],
    'html-body': '<div>This is the actual mail contents, preferably
cleanup html etc in a neutral way</div>',
    }

Note that this json is just an example, it should be something that
contains what is in the message examples.

Safe-HTML-view is special! It should clean up html so that it does not
include scripts, css and any external resources. Probably best to use
some cleanup library etc.

For bonus points, find a way to prevent external images from being
loaded, but make it possible to enable loading them using a browser
button.

Note that if a message is plaintext, the html view should also do a
very simple rendering.

Tip: use Python 3's builtin email module for parsing the bodies!
