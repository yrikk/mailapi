from mailapi.core.elements import Message, MessageHeader
from mailapi.core.models import Message as MessageModel
from mailapi.core.parser import MessageParser
from mailapi.core.sanitizer import HtmlSanitizer


class MessageManagerException(Exception):
    pass


class MessageManager():

    @classmethod
    def get_message(cls, message_id):
        """
        Get message detail.
        Get message by id, parse raw email content, sanitize email body html.
        """
        try:
            obj = MessageModel.objects.get(pk=message_id)
        except MessageModel.DoesNotExist as e:
            raise MessageManagerException(e)

        return cls._prepare_message(obj)

    @classmethod
    def get_messages(cls):
        """
        Get all messages for search query.

        TODO: add filters for user/inbox, limit/offset.
        """
        objects = MessageModel.objects.all()
        return [cls._prepare_message(obj) for obj in objects]

    @staticmethod
    def _prepare_message(obj):
        """
        Internal function to call helper classes
        """
        data = MessageParser.parse(obj.content)
        html_body = HtmlSanitizer.prepare_html(
            data['body'], data['content_type']
        )

        headers = [MessageHeader(key, value) for key, value in data['headers']]
        return Message(
            database_id=obj.id,
            message_id=data['message_id'],
            sender_email=data['sender_email'],
            sender_name=data['sender_name'],
            subject=data['subject'],
            to=data['to'],
            cc=data['cc'],
            headers=headers,
            html_body=html_body,
        )
