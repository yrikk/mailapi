import bleach
import textile
from bs4 import BeautifulSoup

# TODO: tune tags & attributes we allow, e.g. allow styles tag.
# TODO: do not expose css styling to other elements on a page
# See https://litmus.com/help/email-clients/rendering-engines/

ALLOWED_TAGS = [
    'a', 'abbr', 'acronym', 'b', 'blockquote', 'code', 'em',
    'i', 'li', 'ol', 'strong', 'ul', 'img', 'p', 'br',
    'table', 'tr', 'td', 'tbody', 'thead', 'th', 'span',
]
ALLOWED_ATTRIBUTES = {
    'a': ['href', ], 'img': ['src', 'width', 'height', 'alt', ],
    '*': ['class', 'title'],
}
ALLOWED_STYLES = [
    'color', 'font-weight', 'width', 'height',
]
TRANSPARENT_PIXEL = "".join([
    "data:image/gif;base64,",
    "R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",
])


class HtmlSanitizer():

    @classmethod
    def prepare_html(cls, content, content_type):
        """
        1. Sanitize html for html/text messages
        2. Convert plain text messages to html
        3. Hide images
        """
        if not content:
            return None

        if content_type == 'text/plain':
            content = cls.make_html(content)

        # Call sanitize on plain text too, do not trust headers
        content = cls.sanitize_html(content)
        content = cls.hide_images(content)
        return content

    @classmethod
    def sanitize_html(cls, html):
        if not html:
            return None

        # Remove some tags manually because bleach leaves content of the
        # removed tags
        html = cls.remove_tags(html, ['script', 'style'])

        html = bleach.clean(
            html,
            tags=ALLOWED_TAGS,
            attributes=ALLOWED_ATTRIBUTES,
            styles=ALLOWED_STYLES,
            strip=True,
            strip_comments=True,
        )
        return html

    @staticmethod
    def remove_tags(html, tags):
        if not html:
            return None

        soup = BeautifulSoup(html, "html.parser")
        for tag in tags:
            [item.extract() for item in soup.findAll(tag)]
        html = str(soup)
        return html

    @staticmethod
    def make_html(text):
        if not text:
            return None

        return textile.textile(text)

    @staticmethod
    def hide_images(html):
        if not html:
            return None
        # Use "html.parser: to keep page without adding body & head tags
        # as other backends do
        soup = BeautifulSoup(html, "html.parser")
        for img in soup.findAll('img'):
            img['original-src'] = img.get('src')
            img['src'] = TRANSPARENT_PIXEL
        html = str(soup)
        return html
