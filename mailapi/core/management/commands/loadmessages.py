import os
from django.core.management.base import BaseCommand

from mailapi.core.models import Message


class Command(BaseCommand):
    help = 'Load folder with emails as text files into database'

    def add_arguments(self, parser):
        parser.add_argument(
            'path',
            type=str,
            help='Path to the directory with emails',
        )

    def handle(self, *args, **options):
        for root, dirs, files in os.walk(options['path']):
            for file_path in files:
                if file_path.startswith('.'):
                    continue
                with open(os.path.join(root, file_path), "r") as file_obj:
                    Message.objects.create(content=file_obj.read())
                    self.stdout.write('Loaded: {}'.format(file_path))

        self.stdout.write(self.style.SUCCESS('Successfully loaded emails'))
