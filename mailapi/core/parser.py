from email.parser import Parser
from email.utils import parseaddr, getaddresses
from email.header import decode_header
from email.message import EmailMessage


class MessageParser():
    # TODO: handle attachments
    # TODO: consider usage of https://github.com/mailgun/flanker

    @classmethod
    def parse(cls, email_content):
        obj = Parser(_class=EmailMessage).parsestr(text=email_content)
        sender_name, sender_email = parseaddr(obj['from'])

        return {
            'body': cls.get_body(obj),
            'cc': getaddresses(obj.get_all('cc', [])),
            'content_type': cls.get_body_content_type(obj),
            'headers': obj.items(),
            'message_id': obj.get('message-id'),
            'sender_email': sender_email,
            'sender_name': sender_name,
            'subject': cls.get_subject(obj),
            'to': getaddresses(obj.get_all('to', [])),
        }

    @classmethod
    def get_body(cls, obj):
        body_candidate = obj.get_body(preferencelist=('html', 'plain'))
        if body_candidate:
            payload = body_candidate.get_payload(decode=True)
            if payload:
                encoding = cls.get_charset(body_candidate)
                return payload.decode(encoding)
        return None

    @staticmethod
    def get_body_content_type(obj):
        return obj.get_body().get_content_type() if obj.get_body() else None

    @staticmethod
    def get_charset(obj, default_encoding="utf-8"):
        if obj.get_content_charset():
            return obj.get_content_charset()

        if obj.get_charset():
            return obj.get_charset()

        return default_encoding

    @staticmethod
    def get_subject(obj, default_encoding='utf-8'):
        if obj['Subject'] is None:
            return None

        subject = ''
        for text, encoding in decode_header(obj['Subject']):
            if encoding:
                subject += text.decode(encoding)
            else:
                if isinstance(text, str):
                    subject += text
                else:
                    subject += text.decode(default_encoding)

        return subject
