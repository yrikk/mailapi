from collections import namedtuple


Message = namedtuple(
    'Message',
    [
        'database_id',
        'message_id',
        'to',
        'cc',
        'sender_email',
        'sender_name',
        'subject',
        'headers',
        'html_body',
    ]
)

MessageHeader = namedtuple(
    'MessageHeader',
    [
        'key',
        'value',
    ]
)
