from django.conf.urls import url
from django.views.generic.base import TemplateView

from mailapi.api.views import MessageListView, MessageDetailView


urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html')),
    url(r'^api/v1/messages/$', MessageListView.as_view()),
    url(
        r'^api/v1/messages/(?P<message_id>[0-9]+)/$',
        MessageDetailView.as_view()
    ),
]
