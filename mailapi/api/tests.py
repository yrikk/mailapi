import unittest
import json

from email.mime.text import MIMEText

from django.test import Client

from mailapi.core.models import Message

# TODO: write real unittests with pytest


class APITest(unittest.TestCase):

    def setUp(self):
        self.client = Client()
        self.message = Message.objects.create(
            content=self._make_sample_message()
        )

    def tearDown(self):
        Message.objects.all().delete()

    def _make_sample_message(self):
        msg = MIMEText('text')
        msg['Subject'] = 'subject'
        msg['From'] = 'from@example.com'
        msg['To'] = 'to@example.com'
        return msg.as_string()

    def test_details(self):
        url = '/api/v1/messages/{}/'.format(self.message.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.content)
        assert data['from']['address'] == 'from@example.com'
        assert data['subject'] == 'subject'

    def test_list(self):
        url = '/api/v1/messages/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.content)
        assert data['entries'] == [self.message.id]
