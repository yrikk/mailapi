from django.http import JsonResponse
from django.views.generic import View

from mailapi.core.message_manager import (
    MessageManager,
    MessageManagerException,
)


class MessageListView(View):

    def get(self, request):
        """
        Returns list of message ids

        # TODO: implement limit, offset, filters
        """
        messages = MessageManager.get_messages()
        data = {
            'entries': [message.database_id for message in messages],
        }
        return JsonResponse(data)


class MessageDetailView(View):

    def get(self, request, message_id):
        """
        Returns details of a message with given `message_id`
        """
        try:
            message = MessageManager.get_message(message_id)
        except MessageManagerException:
            return JsonResponse(
                {'error': 'Messages with id: {} not found'.format(message_id)},
                status=404,
            )

        data = {
            'message-id': message.message_id,
            'from': {
                'address': message.sender_email,
                'name': message.sender_name,
            },
            'to': [dict(address=item[1], name=item[1]) for item in message.to],
            'cc': [dict(address=item[1], name=item[1]) for item in message.cc],
            'subject': message.subject,
            'raw-headers': [[key, value] for key, value in message.headers],
            'html-body': message.html_body,
        }
        return JsonResponse(data)
